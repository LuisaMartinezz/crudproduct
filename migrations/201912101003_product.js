exports.up = knex => 

knex.schema.createTable('product', (table) => {
    table.increments('id').unsigned().notNullable();
    table.text('code').unsigned().notNullable();
    table.text('nameproduct');
    table.double('cost');
    table.integer('typeproduct').notNullable()
    .references('typeProduct.id');
    
    table.timestamps(true,true);
    table.integer('warehouse').notNullable()
    
});
exports.down = knex => knex.schema.dropTable('product');