const productController = module.exports;
const productService = require('../services/productService.js');
const validator = require('../validators/validator');
const productSchema = require('../validators/productSchema');


productController.createProduct = async (req,res) => {
    const {body} = req;
    try {
        validator.syntax(body,productSchema);
        
        const response = await productService.createProduct(body);
       
        res.status(response.status).json(response.body);

    } catch (error) {console.log(error);
    
        res.status(400).json(error);
        
    }
}

productController.deleteProduct = async (req,res) => {
    const {params} = req;
    productService.deleteProduct(params.code);
    
    return res.send({message:'product Deleted'});
}


productController.updateProduct = async (req,res) => {
    const {body} = req;

    try {
        
        validator.syntax(body,productSchema);
        const response = await productService.updateProduct(body);
        res.status(response.status).json(response.body);
    } catch (error) {
        res.status(400).json(error);
    }
};


productController.getProduct = async (req,res) => {
    return productService.getProduct()
    .then(response => res.send(response));
};


productController.getProductByCode = async (req,res) => {
    const {params} = req;
    return productService.getProductByCode(params.code)
    .then(response => res.send(response));
};










