const productservice = module.exports;
const productRepository = require('../Repositories/productRepository');
const Response = {};

productservice.createProduct = async (product) => {
    Response.status = 400;

    try {
    
        let res = await productRepository.countProduct(product.code);console.log(res);
        if(res.count==0) {
            res = await productRepository.createProduct(product);
            console.log(res);
            
            Response.status=200;
            Response.body=res[0];
        }else{
            Response.status = 401;
            Response.status = 200;
            Response.body = {};
        }

        return Response;

    } catch (error) {console.log(error);
    
        return Response;
    }
}


productservice.deleteProduct = async (code) => {
    Response.status = 400;


     try {
        productRepository.deleteProduct(code);
     } catch (error) {
         return Response;
     }
   
}

productservice.updateProduct = async (product) => {
    Response.status = 400;

    try { 
    let res = await productRepository.countProduct(product.code);
        if(res.count==1) {
            res = await productRepository.updateProduct(product,product.code);
            console.log(res);
            
            Response.status=200;
            Response.body=res[0];
        }else{
            Response.status = 401;
            Response.status = 200;
            Response.body = {};
        }

        return Response;

    } catch (error) {console.log(error);
    
        return Response;
    }
};


productservice.getProduct = async () => {
    return productRepository.getProduct();
};


productservice.getProductByCode = async (code) => {
    Response.status = 400;

    try { 
    let res = await productRepository.countProduct(code);
        if(res.count==1) {
            res = await productRepository.getProductByCode(code);
            console.log(res);
            
            Response.status=200;
            Response.body=res[0];
        }else{
            Response.status = 401;
            Response.status = 200;
            Response.body = {};
        }

        return Response;

    } catch (error) {console.log(error);
    
        return Response;
    }
};
