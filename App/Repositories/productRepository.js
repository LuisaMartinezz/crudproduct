const productRepository = module.exports;
const DB = require('../utils/DB');
productRepository.createProduct = async (product) => await DB('product').insert(product).returning('*');

productRepository.countProduct = async (code) => await DB('product').count('*').where({code:code}).first();

productRepository.deleteProduct = async (code) => await DB('product').where('code',code).delete();

productRepository.updateProduct = async (product,code) => await DB('product').where('code',code).update(product).returning('*');

productRepository.getProduct = () =>  DB.select('*').from('product');

productRepository.getProductByCode = async (code) => DB('product').select('*').where('code',code);