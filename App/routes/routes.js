const Routes = require('express').Router();
const productController = require('../controllers/productController');

Routes.post('/create',productController.createProduct);
Routes.delete('/delete/:code',productController.deleteProduct);
Routes.put('/update',productController.updateProduct);
Routes.get('/product',productController.getProduct);
Routes.get('/product/:code',productController.getProductByCode);


module.exports = Routes;
