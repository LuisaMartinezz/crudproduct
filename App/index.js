const express = require('express');
const app= express();
const morgan = require('morgan');

const routes = require('./routes/routes');
const {PORT = 4000 } = process.env;

app.use(express.json());
app.use(morgan('dev'));
app.use('/api',routes);

app.listen (PORT,() => {
     console.log('listen port:',PORT);
});

module.exports = app;





